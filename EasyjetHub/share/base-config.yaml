# Decay mode considered for truth decoration
truthDecayModes: []

# Toggles for object types
do_event_cleaning: true
do_small_R_jets: false
do_large_R_Topo_jets: false
do_large_R_UFO_jets: false
do_jet_parent_decoration: true
do_VR_jets: false
do_muons: false
do_electrons: false
do_photons: false
do_taus: false
do_met: false
do_mmc: false
save_HF_classification: false

do_bbtt_analysis: false

# tempo fix for adding this flag in main_sequence_config.py
do_bbyy_analysis: false

# Toggles for reconstructed objects decorations
small_R_jet:
  min_pT: 20000
  max_eta: 4.5
  max_eta: 100
  # nominal btagging working point, used for b-jet pT calibration in particular
  btag_wp: ""
  # additional btagging working points, available for selections and per-jet SFs
  btag_extra_wps: []
  runBJetPtCalib: True
  # compute experimental btag variables
  doBtagExpVars: False
  # select jet type
  jet_type: 'reco4PFlowJet'
  # calib tool options
  calibToolCalibArea: ""
  calibToolConfigFile: ""
  calibToolCalibSeq: ""
  # uncertainties tool options
  uncertToolCalibArea: ""
  uncertToolConfigPath: ""
  uncertToolMCType: ""
  amount: 0
  amount_bjet: 0
  variables_bjets: ["pt", "eta", "phi", "E"]
  variables_allJets: ["pt", "eta", "phi", "E"]
  variables_int_bjets: []
  variables_int_allJets: []

large_R_jet:
  # btagging working points for Variable Radius Track jets
  vr_btag_wps: []

Photon:
  ID: ""
  Iso: ""
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  forceFullSimConfig: false
  extra_wps: []

Electron:
  min_pT: 4500
  max_eta: 2.47
  trackSelection: true
  maxD0Significance: 5.
  maxDeltaZ0SinTheta: 0.5
  chargeIDSelection: false
  ID: ""
  Iso: ""
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  forceFullSimConfig: false
  extra_wps: []

Muon:
  min_pT: 3000
  max_eta: 2.5
  trackSelection: true
  maxD0Significance: 3
  maxDeltaZ0SinTheta: 0.5
  ID: ""
  Iso: ""
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  extra_wps: []

Tau:
  ID: ""
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: ["charge", "nProng", "decayMode"]
  extra_wps: []

Lepton:
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: ["charge", "pdgid"]

# list of triggers to apply
# "Auto" configures mc from the r-tag, data from year, e.g. data22
trigger_year: Auto
# list of triggers per year to consider
trigger:
  selection:
    chains:
      "2015": []
      "2016": []
      "2017": []
      "2018": []
      "2022": []
      "2023": []

  scale_factor:
    doSF: False

# apply trigger lists to filter events
do_trigger_filtering: false
# use trigger selections in offline algorithms
do_trigger_offline_filtering: true
# list of directories to search for files
grl_years:
  include: grl_years.yaml
# good runs lists to filter events
grl_files:
  include: general_grl.yaml
# In general we should not need custom Pileup Reweighting
# Uses grl_years to find files
prw_files: {}
# We may need special LumiCalc files depending on triggers
# Uses grl_years to find files
lumicalc_files: {}
# apply loose jet cleaning only
loose_jet_cleaning: false
# disable CP Algs for calibration
disable_calib: false
# Whether to activate CP alg systematic variations
do_CP_systematics: false
systematics_regex: []
systematics_suffix_separator: "_"
# write objects with overlap removal applied
# turning this on you have to decide on one large R jet collection
do_overlap_removal: false
# Enable specific TauAntiTauJet overlap removal, mostly for bbtt analysis
doTauAntiTauJetOR: false
# Enable overlap removal between large-R jets and all objects
do_large_R_jets_OR: true
# Enable overlap removal between small-R jets and large-R jets
# Athena default is true
do_small_R_jet_large_R_jet_OR: true

# Store high level variables by default 
# when the config file set the high level variables
store_high_level_variables: false

# list of DSID of samples for which to store WLep related information
# for overlap removal between samples
DSID_nWLep_samples:
  - 410470 #nonallhad ttbar
  - 601352 #dyn scale Wt DR
  - 601355 #dyn scale Wt DR
  - 601627 #dyn scale Wt DS
  - 601631 #dyn scale Wt DS
  - 601626 #hdamp variations DS
  - 601630 #hdamp variations DS
  - 601460 #hdamp variations DR
  - 601462 #hdamp variations DR
  - 602235 #PS variation
  - 602236 #PS variation
  - 602689 #pThard variation
  - 602690 #pThard variation

# Definition of container names
full_container_names:
  phys:
    include: container-names-DAOD_PHYS.yaml
  physlite:
    include: container-names-DAOD_PHYSLITE.yaml

histograms: false
h5:
  n_jets: 10

ttree_output: []
